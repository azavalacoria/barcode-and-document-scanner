//
//  ViewController.h
//  BarCodeReader
//
//  Created by Adrián Zavala Coria on 12/05/16.
//  Copyright © 2016 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>
//@import AVFoundation;

@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *barCodeField;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;



- (IBAction)captureCode:(id)sender;

@property NSString *barCodeType;
@property NSString *barCode;

@end

