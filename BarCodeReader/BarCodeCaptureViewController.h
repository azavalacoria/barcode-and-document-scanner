//
//  BarCodeCaptureViewController.h
//  BarCodeReader
//
//  Created by Adrián Zavala Coria on 12/05/16.
//  Copyright © 2016 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>

@import AVFoundation;

@interface BarCodeCaptureViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (strong, nonatomic) AVCaptureDevice* device;
@property (strong, nonatomic) AVCaptureDeviceInput* input;
@property (strong, nonatomic) AVCaptureMetadataOutput* output;
@property (strong, nonatomic) AVCaptureSession* session;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer* preview;

@property (nonatomic) BOOL codeDetected;

@property (nonatomic) NSString *barCodeType;

@property (nonatomic) NSString *barCode;

@end
