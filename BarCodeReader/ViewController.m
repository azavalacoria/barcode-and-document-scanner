//
//  ViewController.m
//  BarCodeReader
//
//  Created by Adrián Zavala Coria on 12/05/16.
//  Copyright © 2016 Personal. All rights reserved.
//

#import "ViewController.h"
#import "BarCodeCaptureViewController.h"

@interface ViewController ()

@end

@implementation ViewController


@synthesize barCodeField;
@synthesize imageView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //[self setCodeDetected:NO];
    
}

- (void)viewDidAppear:(BOOL)animated {
    if (_barCode != nil) {
        [barCodeField setText:_barCode];
    } else {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"loadCamera"]) {
        BarCodeCaptureViewController *barCode;
        barCode  = [segue destinationViewController];
    }
}


- (IBAction)captureCode:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Opciones" message:@"Seleccione una acción:" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *curp = [UIAlertAction actionWithTitle:@"Escanear CURP"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self performSegueWithIdentifier:@"loadCamera" sender:self];
                                            }];
    [alert addAction:curp];
    
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"Tomar fotografía"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action) {
                                                     [self loadCamera];
                                                 }];
    [alert addAction:photo];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar"
                                                    style:UIAlertActionStyleDestructive
                                                  handler:^(UIAlertAction * action) {
                                                      
                                                  }];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (void)loadCamera {
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    [pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    [pickerController setDelegate:self];
    [self presentViewController:pickerController animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = info [UIImagePickerControllerOriginalImage];
    
    
    [imageView setImage:image];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    /*
    [imageView setUserInteractionEnabled:YES];
    UITapGestureRecognizer *single = [[UITapGestureRecognizer alloc] 
                                        initWithTarget:self action:@selector(loadCamera)];
    [single setNumberOfTapsRequired:1];
    [imageView addGestureRecognizer:single];
    */
}

@end
