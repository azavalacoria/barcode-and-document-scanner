//
//  DocumentScannerViewController.h
//  BarCodeReader
//
//  Created by Adrián Zavala Coria on 12/05/16.
//  Copyright © 2016 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPDFCameraViewController.h"

@interface DocumentScannerViewController : UIViewController

@property (weak, nonatomic) IBOutlet IPDFCameraViewController *cameraViewController;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *captureButton;

- (IBAction)capturePhoto:(id)sender;
@end
