//
//  BarCodeCaptureViewController.m
//  BarCodeReader
//
//  Created by Adrián Zavala Coria on 12/05/16.
//  Copyright © 2016 Personal. All rights reserved.
//

#import "BarCodeCaptureViewController.h"
#import "ViewController.h"

@interface BarCodeCaptureViewController ()

@end

@implementation BarCodeCaptureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setCodeDetected:NO];
    [self capureBarCode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)capureBarCode {
    // Inicializamos la sesión de captura de video
    self.session = [[AVCaptureSession alloc] init];
    
    // Configuramos como dispositivo la cámara de video
    self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Configuramos como dispositivo de entrada la cámara recien inicializada
    self.input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    [self.session addInput:self.input];
    
    // Configuramos como salida la captura de metadatos (para lectura de códigos)
    self.output = [[AVCaptureMetadataOutput alloc] init];
    [self.output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [self.session addOutput:self.output];
    
    // Configuramos como tipos de código a detectar, todos los disponibles
    // Es posible unicamente recoger códigos de unos tipos determinados
    self.output.metadataObjectTypes = [self.output availableMetadataObjectTypes];
    
    // Inicializamos una capa de previsualización para poder ver lo que la
    // cámara de video captura
    self.preview = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    self.preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.preview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view.layer insertSublayer:self.preview atIndex:0];
    
    // Lanzamos la sesión de cámara
    [self.session startRunning];
}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    if (!self.codeDetected) {
        
        // Recorremos los metadatos obtenidos
        for (AVMetadataObject *metadata in metadataObjects) {
            
            // Recuperamos el valor textual del código de barras o QR
            NSString *code =[(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            
            // Si el código no esta vacío
            if (![code isEqualToString:@""]) {
                
                // Marcamos nuestro flag de detección a YES
                self.codeDetected = YES;
                
                // Mostramos al usuario un alert con los datos del tipo de código y valor
                // detectado en nuestra sesión
                [self.session stopRunning];
                //NSString *string = [NSString stringWithFormat:@"%@ - %@", metadata.type, code];
                [self setBarCodeType: metadata.type];
                [self setBarCode:code];
                [self setCodeDetected: NO];
                [self performSegueWithIdentifier:@"toPresentController" sender:self];
            }
        }
        
    }
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"toPresentController"]) {
        ViewController *view;
        view = [segue destinationViewController];
        [view setBarCode:_barCode];
        [view setBarCodeType:_barCodeType];
    }
    
}


@end
